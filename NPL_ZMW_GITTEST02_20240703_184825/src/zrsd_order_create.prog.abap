*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*& Report ZRSD_ORDER_CREATE
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zrsd_order_create.
*&TRY.
TYPES:
  BEGIN OF ts_subdata,
    key   TYPE string,
    value TYPE string,
  END OF ts_subdata,
  tt_subdata TYPE STANDARD TABLE OF ts_subdata WITH EMPTY KEY,

  BEGIN OF ts_data,
    text   TYPE string,
    number TYPE i,
    bool   TYPE abap_bool,
    table  TYPE tt_subdata,
  END OF ts_data,

  BEGIN OF ts_result,
    status             TYPE string,
    external_reference TYPE string,
    external_status    TYPE string,
    external_message   TYPE string,
  END OF ts_result,


  BEGIN OF total_count,
    total_count TYPE i,
  END OF total_count,
  tt_total_count TYPE STANDARD TABLE OF total_count.

DATA: lo_http_client TYPE REF TO if_http_client.
DATA: response        TYPE string,
      lv_http_status  TYPE i,
      lv_http_text    TYPE string,
      ls_json_tab     TYPE ZRSD_order_confirmations_get,
      lr_data         TYPE REF TO data,
      lt_subdata      TYPE tt_subdata,
      lv_content_type TYPE string,
      lv_response     TYPE string,
      lt_total_count  TYPE TABLE OF total_count,
      ls_total_count  TYPE total_count,
      ls_result       TYPE ts_result,
      lt_result       TYPE TABLE OF ts_result,
      lv_json_result  TYPE string,
      lv_url          TYPE string.
FIELD-SYMBOLS:
  <lt_table> TYPE STANDARD TABLE.

DATA: lo_order_workist TYPE REF TO ZRSD_cl_order_workist.

DATA:
  gt_return TYPE ZRSD_answer_tab,
  gs_return TYPE ZRSD_answer,
  gv_url    TYPE string.

DATA: gv_bearer_new TYPE string.

SELECTION-SCREEN BEGIN OF BLOCK rad1
  WITH FRAME TITLE title.                     " Master Data Import
  PARAMETERS:
    p_channe TYPE ZRSD_channel DEFAULT '60e59564-d6f8-4381-8dc2-585ee726f198'     NO-DISPLAY,
    p_bearer TYPE ZRSD_bearer  DEFAULT 'b73a2ded0e989c6b5578d7a6f21850c2edb1df1a' NO-DISPLAY.
  SELECTION-SCREEN SKIP.

  PARAMETERS:
    p_vkorg TYPE vkorg OBLIGATORY.
SELECTION-SCREEN END OF BLOCK rad1.


*--------------------------------------------------------------------*
* Initialization
*--------------------------------------------------------------------*
INITIALIZATION.
  title = TEXT-000. "'Order Creation'(000).




*--------------------------------------------------------------------*
* Start of Selection
*--------------------------------------------------------------------*
START-OF-SELECTION.

  SELECT channel,
         bearer
    FROM ZRSD_sales_order
    INTO (@p_channe,
          @p_bearer)
    WHERE vkorg EQ @p_vkorg
    AND   sysid EQ @sy-sysid.
  ENDSELECT.

* Syncwork Test-Channel 60e59564-d6f8-4381-8dc2-585ee726f198
*  gv_url = '/v1/orders?channel=' && p_channe && '&finished=true&imported=false'.
* zum Test channele aus KaTe
*  p_channe = "60e59564-d6f8-4381-8dc2-585ee726f198".
*  p_bearer = "b73a2ded0e989c6b5578d7a6f21850c2edb1df1a".

  gv_url = '/v1/orders?channel=' && p_channe && '&finished=true&imported=false'.

*---------------------------------------------------------------------*
* Start program
*---------------------------------------------------------------------*

  DATA: lv_rfcdest TYPE rfcdest VALUE 'WORKIST'.

  CALL METHOD cl_http_client=>create_by_destination   "BH
    EXPORTING
      destination              = lv_rfcdest
    IMPORTING
      client                   = lo_http_client
    EXCEPTIONS
      argument_not_found       = 1
      destination_not_found    = 2
      destination_no_authority = 3
      plugin_not_active        = 4
      internal_error           = 5
      OTHERS                   = 6.

  IF sy-subrc <> 0.
* Implement suitable error handling here
  ENDIF.

  CALL METHOD cl_http_utility=>set_request_uri   "BH
    EXPORTING
      request = lo_http_client->request
      uri     = gv_url.

**create HTTP client by url
*  CALL METHOD cl_http_client=>create_by_url
*    EXPORTING
*      url                = gv_url
**     url                = 'https://api.workist.com/v1/orders?channel=60e59564-d6f8-4381-8dc2-585ee726f198&finished=true&imported=false'
*    IMPORTING
*      client             = lo_http_client
*    EXCEPTIONS
*      argument_not_found = 1
*      plugin_not_active  = 2
*      internal_error     = 3
*      OTHERS             = 4.
*
*  IF sy-subrc <> 0.
**error handling
*  ENDIF.

  IF lo_http_client IS BOUND.
* HTTP-Prtotokoll-Version
    lo_http_client->request->set_version( version = if_http_request=>co_protocol_version_1_1 ).

*setting request method
    lo_http_client->request->set_method( if_http_request=>co_request_method_get ).

*adding headers
    CONCATENATE 'Bearer ' p_bearer
           INTO gv_bearer_new
      SEPARATED BY space.
    lo_http_client->request->set_header_field( name = 'Content-Type' value = 'application/json' ).
    lo_http_client->request->set_header_field( name = 'Authorization' value = gv_bearer_new ).
    lo_http_client->request->set_header_field( name = 'accept' value = 'application/json').

  ENDIF.

*;:
  CALL METHOD lo_http_client->send
    EXCEPTIONS
      http_communication_failure = 1
      http_invalid_state         = 2
      http_processing_failed     = 3
      http_invalid_timeout       = 4
      OTHERS                     = 5.

  IF sy-subrc = 0.
    CALL METHOD lo_http_client->receive
      EXCEPTIONS
        http_communication_failure = 1
        http_invalid_state         = 2
        http_processing_failed     = 3
        OTHERS                     = 5.
  ENDIF.

  lo_http_client->response->get_status(
    IMPORTING
      code   = lv_http_status " HTTP Status Code
      reason = lv_http_text " HTTP Status Beschreibung
  ).

  lo_http_client->response->get_content_type(
    RECEIVING
      content_type = lv_content_type
  ).

  IF sy-subrc <> 0.
*error handling
  ENDIF.

  WRITE:/ 'HTTP STATUS CODE:', lv_http_status.
  WRITE:/ 'STATUS TEXT:', lv_http_text.

  IF lv_http_status = 200.
    response = lo_http_client->response->get_cdata( ).
  ENDIF.

  WRITE:/ response.


  /ui2/cl_json=>deserialize(
    EXPORTING
      json = response
*     JSONX            =
*     PRETTY_NAME      = /ui2/cl_json=>pretty_mode-low_case
*     ASSOC_ARRAYS     = 'X'
*     ASSOC_ARRAYS_OPT =
*     NAME_MAPPINGS    =
*     CONVERSION_EXITS =
*     HEX_AS_BASE64    =
    CHANGING
      data = ls_json_tab
  ).

  CREATE OBJECT lo_order_workist.

*---------------------------------------------------------------------*
* Order creation
*---------------------------------------------------------------------*
  CALL METHOD lo_order_workist->check_data(
    EXPORTING
      iv_vkorg  = p_vkorg
    CHANGING
      is_data   = ls_json_tab
      et_return = gt_return
  ).

  LOOP AT gt_return INTO gs_return.
    SKIP.
    IF gs_return-document IS NOT INITIAL.
      ls_result-status = 'SUCCEEDED'.
    ELSE.
      ls_result-status = 'FAILED'.
    ENDIF.
    ls_result-external_reference = gs_return-document.
    ls_result-external_status    = ''.
    ls_result-external_message   = gs_return-message.

    /ui2/cl_json=>serialize(
      EXPORTING
        data        = ls_result
*       COMPRESS    =
*       NAME        =
        pretty_name = /ui2/cl_json=>pretty_mode-low_case
*       TYPE_DESCR  =
*       ASSOC_ARRAYS     =
*       TS_AS_ISO8601    =
*       EXPAND_INCLUDES  =
*       ASSOC_ARRAYS_OPT =
*       NUMC_AS_STRING   =
*       NAME_MAPPINGS    =
*       CONVERSION_EXITS =
*       FORMAT_OUTPUT    =
*       HEX_AS_BASE64    =
      RECEIVING
        r_json      = lv_json_result
    ).


    CALL METHOD cl_http_client=>create_by_destination   "BH
      EXPORTING
        destination              = lv_rfcdest
      IMPORTING
        client                   = lo_http_client
      EXCEPTIONS
        argument_not_found       = 1
        destination_not_found    = 2
        destination_no_authority = 3
        plugin_not_active        = 4
        internal_error           = 5
        OTHERS                   = 6.
    IF sy-subrc <> 0.
* Implement suitable error handling here
    ENDIF.

    lv_url = '/v1/orders/' && gs_return-id && '/mark_imported'.

    CALL METHOD cl_http_utility=>set_request_uri   "BH
      EXPORTING
        request = lo_http_client->request
        uri     = lv_url.

    IF lo_http_client IS BOUND.
* HTTP-Prtotokoll-Version
      lo_http_client->request->set_version( version = if_http_request=>co_protocol_version_1_1 ).

*setting request method
      lo_http_client->request->set_method( 'PUT' ).

*adding headers
      CONCATENATE 'Bearer ' p_bearer
             INTO gv_bearer_new
        SEPARATED BY space.
      lo_http_client->request->set_header_field( name = 'Content-Type' value = 'application/json' ).
      lo_http_client->request->set_header_field( name = 'Authorization' value = gv_bearer_new ).
      lo_http_client->request->set_header_field( name = 'accept' value = 'application/json').

    ENDIF.

    lo_http_client->request->set_cdata(
      data = lv_json_result
*     OFFSET = 0                " Offset in Character-Daten
*     LENGTH = -1               " L nge der Character-Daten
    ).

    CALL METHOD lo_http_client->send
      EXCEPTIONS
        http_communication_failure = 1
        http_invalid_state         = 2
        http_processing_failed     = 3
        http_invalid_timeout       = 4
        OTHERS                     = 5.

    IF sy-subrc = 0.
      CALL METHOD lo_http_client->receive
        EXCEPTIONS
          http_communication_failure = 1
          http_invalid_state         = 2
          http_processing_failed     = 3
          OTHERS                     = 5.
    ENDIF.

    lo_http_client->response->get_status(
      IMPORTING
        code   = lv_http_status " HTTP Status Code
        reason = lv_http_text " HTTP Status Beschreibung
    ).

* Alternativ SAP-LOG erstellen
    WRITE:
       / 'ID: ',      015 gs_return-id,
       / 'Channel: ', 015 gs_return-channel,
       / 'Order: ',   015 gs_return-document,
       / 'Message: ', 015 gs_return-message,
       / 'Buchungsstatus: ', 015 ls_result-status.
  ENDLOOP.

*close http connection
  lo_http_client->close(
*  EXCEPTIONS
*    HTTP_INVALID_STATE = 1                " Ung ltiger Zustand
*    OTHERS             = 2
  ).
  IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*   WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.
