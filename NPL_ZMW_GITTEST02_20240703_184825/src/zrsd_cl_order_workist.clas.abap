class ZRSD_CL_ORDER_WORKIST definition
  public
  final
  create public .

public section.

  class-methods ADD_MAIL_TO_ORDER
    importing
      !IV_VBELN type VBELN
      !IV_DOCUMENT type ZRSD_STRING
      !IV_FILENAME type ZRSD_STRING optional
    exporting
      !EV_RETURN type INT4 .
  methods CREATE_ORDER
    importing
      !IV_SALESDOCUMENTIN type ANY
      !IV_ID type ZRSD_ID
      !IV_CHANNEL type ZRSD_CHANNEL
      !IV_SENDER type ANY
      !IV_BINARY_RELATIONSHIPTYPE type ANY
      !IV_INT_NUMBER_ASSIGNMENT type ANY
      !IV_BEHAVE_WHEN_ERROR type ANY
      !IS_LOGIC_SWITCH type ANY
      !IV_TESTRUN type ANY
      !IV_CONVERT type ANY
      !IV_URL_MAIL type ZRSD_STRING
      !IV_URL_PDF type ZRSD_STRING
    exporting
      !EV_SALESDOCUMENT type ANY
      !CT_RETURN type ANY
    changing
      !IT_ORDER_CFGS_REF type ANY
      !IT_ORDER_CFGS_INST type ANY
      !IT_ORDER_CFGS_PART_OF type ANY
      !IT_ORDER_CFGS_VALUE type ANY
      !IT_ORDER_CFGS_BLOB type ANY
      !IT_ORDER_CFGS_VK type ANY
      !IT_ORDER_CFGS_REFINST type ANY
      !IT_ORDER_CCARD type ANY
      !IT_ORDER_TEXT type ANY
      !IT_ORDER_KEYS type ANY
      !IT_EXTENSIONIN type ANY
      !IT_PARTNERADDRESSES type ANY
      !IT_EXTENSIONEX type ANY
      !IT_NFMETALLITMS type ANY
      !IT_ORDER_CONDITIONS_IN type ANY
      !IT_ORDER_CONDITIONS_INX type ANY
      !IT_ORDER_SCHEDULES_INX type ANY
      !IS_ORDER_HEADER_IN type ANY
      !IS_ORDER_HEADER_INX type ANY
      !IT_ORDER_ITEMS_IN type ANY
      !IT_ORDER_ITEMS_INX type ANY
      !IT_ORDER_PARTNERS type ANY
      !IT_ORDER_SCHEDULES_IN type ANY .
protected section.
private section.
ENDCLASS.



CLASS ZRSD_CL_ORDER_WORKIST IMPLEMENTATION.


METHOD add_mail_to_order.
*----------------------------------------------------------
* METHOD ADD_MAIL_TO_ORDER
*----------------------------------------------------------
* IV_VBELN    Importing Type  VBELN
* IV_DOCUMENT Importing Type  ZRSD_STRING
* IV_FILENAME Importing Type  ZRSD_STRING
* EV_RETURN   Exporting Type  INT4
*----------------------------------------------------------

  DATA:
    Lv_object_id  TYPE saeobjid,
    lv_sap_object TYPE saeanwdid,
    iv_ar_object  TYPE saeobjart.

  Lv_object_id  = iv_vbeln.
  lv_sap_object = 'BUS2032'.
*  IV_AR_OBJECT  = 'ZSDIORDERM'.
  iv_ar_object  = 'ZSDIORDEMM'.   "Mail Hr. Hohensee 02.07.24

*  CALL FUNCTION 'ZRSD_ARCHIV_ATT_CREATE'
*    EXPORTING
**     IV_FILE       = IV_FILE
*      iv_bstring    = iv_document
*      iv_object_id  = Lv_object_id
*      iv_sap_object = lv_sap_object
*      iv_ar_object  = iv_ar_object
*      iv_filename   = iv_filename
*    IMPORTING
*      ev_return     = ev_return.

ENDMETHOD.


  METHOD create_order.

  DATA:
"    ls_partner      TYPE bapiparnr,
    ls_partner      TYPE REF TO DATA, "bapiparnr,

    ls_ORDER_DOC    TYPE ZRSD_order_doc,
    LV_DOC_CONTENT  TYPE ZRSD_STRING,
    LV_MAIL_CONTENT TYPE ZRSD_STRING,
    ls_RETURN       TYPE bapiret2,
    lt_RETURN       TYPE TABLE OF bapiret2.

  DATA:
    ls_log          TYPE bal_s_log,
    ls_log_handle   TYPE balloghndl,
    lt_log_handle   TYPE bal_t_logh,
    ls_msg          TYPE bal_s_msg.

  CLEAR: ev_salesdocument.

****************************************************************
* Anbindung Dokumente an SD-Auftrag
****************************************************************
  DATA: lv_return TYPE syst_subrc.

* IF sy-uname EQ 'DE.SYNCWORK'.
****************************************************************
* Anbindung Mail an SD-Auftrag
****************************************************************
    IF NOT iv_url_mail IS INITIAL.

*      CALL METHOD me->GET_MAIL_FROM_URL
*        EXPORTING
*          iv_vbeln       = ev_salesdocument
*          iv_url         = iv_url_mail
*        IMPORTING
*          EV_DOC_CONTENT = LV_MAIL_CONTENT
*          ev_return      = lv_return.
    ENDIF.   "iv_url_mail

****************************************************************
* Anbindung PDF an SD-Auftrag
****************************************************************
    IF NOT iv_url_pdf IS INITIAL.

*      CALL METHOD me->get_document_from_url
*        EXPORTING
*          iv_vbeln       = ev_salesdocument
*          iv_url         = iv_url_pdf
*        IMPORTING
*          EV_DOC_CONTENT = LV_DOC_CONTENT
*          ev_return      = lv_return.

    ENDIF.   "iv_url_pdf
*  ENDIF.

*  CALL FUNCTION 'BAPI_SALESORDER_CREATEFROMDAT2'
*    EXPORTING
**     SALESDOCUMENTIN       = SALESDOCUMENTIN
*      order_header_in       = Is_ORDER_HEADER_IN
*      order_header_inx      = is_order_header_inx
**     SENDER                = SENDER
**     BINARY_RELATIONSHIPTYPE       = BINARY_RELATIONSHIPTYPE
*      int_number_assignment = 'X'
**     BEHAVE_WHEN_ERROR     = BEHAVE_WHEN_ERROR
**     LOGIC_SWITCH          = LOGIC_SWITCH
**     TESTRUN               = TESTRUN
*      convert               = ' '
*    IMPORTING
*      salesdocument         = ev_salesdocument
*    TABLES
*      order_items_in        = it_order_items_in
*      order_items_inx       = it_order_items_inx
*      order_partners        = it_order_partners
*      order_schedules_in    = it_order_schedules_in
*      order_schedules_inx   = it_ORDER_SCHEDULES_INX
**     order_conditions_in   = it_ORDER_CONDITIONS_IN
**     order_conditions_inx  = it_ORDER_CONDITIONS_INX
**     order_cfgs_ref        = it_ORDER_CFGS_REF
**     order_cfgs_inst       = it_ORDER_CFGS_INST
**     order_cfgs_part_of    = it_ORDER_CFGS_PART_OF
**     order_cfgs_value      = it_ORDER_CFGS_VALUE
**     order_cfgs_blob       = it_ORDER_CFGS_BLOB
**     order_cfgs_vk         = it_ORDER_CFGS_VK
**     order_cfgs_refinst    = it_ORDER_CFGS_REFINST
**     order_ccard           = it_ORDER_CCARD
**     order_text            = it_ORDER_TEXT
**     order_keys            = it_ORDER_KEYS
**     extensionin           = it_EXTENSIONIN
**     partneraddresses      = it_PARTNERADDRESSES
**     extensionex           = it_EXTENSIONEX
**     nfmetallitms          = it_NFMETALLITMS
*      return                = ct_return.
*
**DATA WAIT   TYPE BAPITA-WAIT.
**DATA RETURN TYPE BAPIRET2.
*
*



*  CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
** EXPORTING
**   WAIT          = WAIT
*    IMPORTING
*      return = ls_RETURN.

* ++++++++++++++++++++++++++++

  IF NOT ev_salesdocument IS INITIAL.

**  IF sy-uname EQ 'DE.SYNCWORK'.
*    if LV_DOC_CONTENT is not initial.
*      CALL METHOD me->ADD_DOCUMENT_TO_ORDER
*        EXPORTING
*          iv_vbeln    = ev_salesdocument
*          IV_DOCUMENT = LV_DOC_CONTENT
*        IMPORTING
*          ev_return   = lv_return.
*    endif.
*    if LV_MAIL_CONTENT is not initial.
*      CALL METHOD me->ADD_MAIL_TO_ORDER
*        EXPORTING
*          iv_vbeln    = ev_salesdocument
*          IV_DOCUMENT = LV_MAIL_CONTENT
*        IMPORTING
*          ev_return   = lv_return.
*    endif.
**   endif.

*    READ TABLE it_order_partners
*         INTO  ls_partner
*         WITH KEY partn_role = 'AG'.

    IF sy-subrc EQ 0.
      MOVE:
*        ls_partner-partn_numb TO ls_ORDER_DOC-kunnr,
        iv_channel            TO ls_ORDER_DOC-channel,
        iv_ID                 TO ls_ORDER_DOC-id,
        ev_salesdocument      TO ls_ORDER_DOC-vbeln,
        sy-datum              TO ls_ORDER_DOC-erdat,
        sy-uzeit              TO ls_ORDER_DOC-erzet,
        sy-uname              TO ls_ORDER_DOC-ernam.

      INSERT ZRSD_order_doc
        FROM ls_ORDER_DOC.

****************************************************************
** create a log where all messages should be added to
****************************************************************
** define some header data of this log
      CONCATENATE ls_ORDER_DOC-vbeln
                  ls_ORDER_DOC-kunnr
                  ls_ORDER_DOC-id
             INTO ls_log-extnumber
        SEPARATED BY space.
*      ls_log-extnumber = 'Application Log Demo'.            "#EC NOTEXT
      ls_log-object    = 'SD-SLS'.                          "#EC NOTEXT
      ls_log-subobject = 'SDOC_SAVE'.                       "#EC NOTEXT
      ls_log-aluser    = ls_ORDER_DOC-ernam.
      ls_log-alprog    = sy-repid.
** ... see structure BAL_S_LOG for further data ...
** ... which can be added to a log header       ...
** create a log
      CALL FUNCTION 'BAL_LOG_CREATE'
        EXPORTING
          i_s_log      = ls_log
        IMPORTING
          e_log_handle = ls_log_handle
        EXCEPTIONS
          OTHERS       = 1.

      IF sy-subrc <> 0.
*    message id sy-msgid type sy-msgty number sy-msgno
*            with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

*      LOOP AT ct_return
*         INTO ls_return.
** define data of message for Application Log
*        ls_msg-msgty     = ls_RETURN-type.
*        ls_msg-msgid     = ls_RETURN-id.
*        ls_msg-msgno     = ls_RETURN-number.
*        ls_msg-msgv1     = ls_RETURN-message_v1.
*        ls_msg-msgv2     = ls_RETURN-message_v2.
*        ls_msg-msgv3     = ls_RETURN-message_v3.
*        ls_msg-msgv4     = ls_RETURN-message_v4.
**  ls_msg-probclass = i_probclass.
*
** ... see structure BAL_S_LOG or report SBAL_DEMO_02 for ...
** ... further data which can be added to a message       ...
*
** add this message to log file
** we do not specify I_LOG_HANDLE since we want to add this message
** to the default log. If it does not exist we do not care
** (EXCEPTIONS log_not_found = 0).
*        CALL FUNCTION 'BAL_LOG_MSG_ADD'
*          EXPORTING
*            i_log_handle  = ls_log_handle
*            i_s_msg       = ls_msg
**           I_LOG_HANDLE  =
*          EXCEPTIONS
*            log_not_found = 0
*            OTHERS        = 1.
*
*        IF sy-subrc <> 0.
*          MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                  WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*        ENDIF.
*
*      ENDLOOP.
*
*      INSERT ls_log_handle INTO TABLE lt_log_handle.
*
*      CALL FUNCTION 'BAL_DB_SAVE'
*        EXPORTING
*          i_client         = sy-mandt
**         I_IN_UPDATE_TASK = ' '
*          i_save_all       = 'X'
*          i_t_log_handle   = lt_log_handle
**        IMPORTING
**         E_NEW_LOGNUMBERS =
*        EXCEPTIONS
*          log_not_found    = 1
*          save_not_allowed = 2
*          numbering_error  = 3
*          OTHERS           = 4.
*
*      IF sy-subrc EQ 0.
*        REFRESH: lt_log_handle.
*      ENDIF.
*
*    ENDIF.
*
*
*  ENDIF.   "ev_salesdocument
*
*  IF NOT ls_return IS INITIAL.
*    APPEND ls_return
*        TO ct_return.
*  ELSEIF NOT ev_salesdocument IS INITIAL.
*    READ TABLE ct_return
*         TRANSPORTING NO FIELDS
*         WITH KEY type   = 'S'
*                  id     = 'V1'
*                  number = '311'.
*    IF sy-subrc EQ 0.
*      DELETE  ct_return
*        WHERE type   = 'S'
*        AND   id     = 'V4'
*        AND   number = '233'.
   ENDIF.
  ENDIF.


ENDMETHOD.
ENDCLASS.
